# CREATE WEBAPP

1. CREATE Webapp with GUI

![Image](https://i.ibb.co/6Xhtx8X/WEBAPP1.png)


2. CREATE Webapp with CLI

To create a Webapp within Azure with Azure CLI use the following commands:
```
az login

az group create -l westus -n myResourceGroupCli

az appservice plan create -g myResourceGroupCli -n myAppServicePlanCli --sku F1

az webapp create --name myWebAppCli-1 --resource-group myResourceGroupCli --plan myAppServicePlanCli
```


3. CREATE Webapp with POWERSHELL

To create a Webapp within Azure with Azure Powershell use the following commands:

```
Connect-AzAccount #Connect-AzAccount -UseDeviceAuthentication

New-AzResourceGroup -Name myResourceGroupPs -Location "West US"

New-AzAppServicePlan -Name myAppServicePlanPs -ResourceGroupName myResourceGroupPs -Tier F1

New-AzWebApp -Name myWebAppPs -AppServicePlan myAppServicePlanPs -ResourceGroupName myAppServicePlanPs
```

[Azure Register Resource Providers](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/resource-providers-and-types#register-resource-provider-1)
[Azure Resource Providers](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/azure-services-resource-providers)

# CREATE VM AND NETWORK EXTRAS

Azure CLI Create VM:
```
az vm create \
  --resource-group learn-0772cdc6-d022-4c6c-a6c4-cfe7937c3d8e \
  --name my-vm \
  --image UbuntuLTS \
  --admin-username azureuser \
  --generate-ssh-keys
```

Extension set is to setup additional software after a deployment (not cloud init)
```
az vm extension set \
  --resource-group learn-0772cdc6-d022-4c6c-a6c4-cfe7937c3d8e \
  --vm-name my-vm \
  --name customScript \
  --publisher Microsoft.Azure.Extensions \
  --version 2.1 \
  --settings '{"fileUris":["https://raw.githubusercontent.com/MicrosoftDocs/mslearn-welcome-to-azure/master/configure-nginx.sh"]}' \
  --protected-settings '{"commandToExecute": "./configure-nginx.sh"}'
```
Get the IP Adress of Azure VM
```
IPADDRESS="$(az vm list-ip-addresses \
  --resource-group learn-0772cdc6-d022-4c6c-a6c4-cfe7937c3d8e \
  --name my-vm \
  --query "[].virtualMachine.network.publicIpAddresses[*].ipAddress" \
  --output tsv)"
```
List NSG and contents of the choosen NSG
```
az network nsg list \
  --resource-group learn-0772cdc6-d022-4c6c-a6c4-cfe7937c3d8e \
  --query '[].name' \
  --output tsv
```
```
az network nsg rule list \
  --resource-group learn-0772cdc6-d022-4c6c-a6c4-cfe7937c3d8e \
  --nsg-name my-vmNSG
```

Create NSG Rule to access the VM via port 80:
```
az network nsg rule create \
  --resource-group learn-0772cdc6-d022-4c6c-a6c4-cfe7937c3d8e \
  --nsg-name my-vmNSG \
  --name allow-http \
  --protocol tcp \
  --priority 100 \
  --destination-port-range 80 \
  --access Allow
```
List NSG Rules:
```
az network nsg rule list \
  --resource-group learn-0772cdc6-d022-4c6c-a6c4-cfe7937c3d8e \
  --nsg-name my-vmNSG \
  --query '[].{Name:name, Priority:priority, Port:destinationPortRange, Access:access}' \
  --output table
Name               Priority    Port    Access
-----------------  ----------  ------  --------
default-allow-ssh  1000        22      Allow
allow-http         100         80      Allow
```

## CREATE STORAGE ACCOUNT

![Create SAS](https://i.imgur.com/yK2tiZA.png)
![Login SAS](https://i.imgur.com/OQKyXJh.png)

Use the SAS Token and Azure CLI to upload/download items.
Tokens can be created within the Storage Account and adjusted for your specific needs.
AZ-CLI Documents: [LINK](https://learn.microsoft.com/en-us/cli/azure/storage/container?source=recommendations&view=azure-cli-latest)
```
az storage account list 
"name": "tbzstorage",

az storage container list --account-name tbzstorage
"name": "storage1",
"name": "storage2",

az storage container generate-sas \
  --account-name tbzstorage \
  --name storage1 \
  --permissions acdlrw \
  --expiry 2023-03-28 \
```
Up and Download files with the created SAS Token with AZ-CLI
```
az storage blob [upload/download]
  --container-name storage1 
  --account-name tbzstorage 
  --file test.PNG 
  --sas-token "XXXX"
```
```
- In welchen Fällen sollte man Account SAS verwenden?
1. Wenn Sie temporären Zugriff auf Ihre Ressourcen gewähren möchten. Sie können ein Ablaufdatum und eine Uhrzeit für den SAS-Token festlegen.
2. Wenn Sie Zugriff auf spezifische Ressourcen in Ihrem Storage-Konto gewähren möchten, beispielsweise nur auf eine bestimmte Dateifreigabe oder einen bestimmten Blob-Container.
3. Wenn Sie den Zugriff auf Ihre Ressourcen von einer bestimmten IP-Adresse aus beschränken möchten.
4. Wenn Sie den Zugriff auf Ihre Ressourcen auf Lesezugriff beschränken möchten.
5. Wenn Sie einen SAS-Token an Dritte weitergeben möchten, z.B. an Partner oder Kunden, ohne ihnen vollständige Zugriffsrechte auf Ihr Konto zu gewähren.
- Welche andere Connection-Möglichkeiten bietet das Tool Azure Storage Explorer an?
Ja, Azure Storage Explorer bietet verschiedene Verbindungsmöglichkeiten, einschließlich der Verwendung von Azure Active Directory, Verbindungsschlüsseln, lokaler Emulation und HTTP/HTTPS-Protokollen. Dadurch können Sie auf Azure Storage-Konten und -Ressourcen zugreifen und diese verwalten.
- Weshalb gibt es bei storage account zwei signing keys?
Die beiden Zugriffsschlüssel ermöglichen eine einfache Regeneration und Rotation ohne Beeinträchtigung der Anwendungsverfügbarkeit.
- Gibt es Empfehlungen/ best practices für die Verwendung von SAS/ Storage keys?
1. Verwenden Sie SAS-Token für granulare Kontrollen und Storage Keys nur, wenn Sie Vollzugriff benötigen.
2. Speichern Sie keine Storage Keys in Klartext oder öffentlich zugänglichen Quellcode-Repositories.
3. Verwenden Sie lange und komplexe Storage Key- und SAS-Token-Werte.
4. Rotieren Sie Ihre Storage Keys regelmäßig.
5. Verwenden Sie Azure Key Vault, um Ihre Storage Keys sicher zu speichern und zu verwalten.
6. Begrenzen Sie die Anzahl der SAS-Token, die an externe Benutzer weitergegeben werden, und stellen Sie sicher, dass sie nur für den benötigten Zeitraum gültig sind.
7. Verwenden Sie SAS-Token, um den Zugriff auf bestimmte Ressourcen in Ihrem Storage-Konto zu beschränken.
- Beischreibe, wie du die Anforderung least privilege umgesetzt hast.
1. Verwenden Sie Azure Active Directory (AAD) zur Authentifizierung und Autorisierung von Benutzern und Anwendungen.
2. Verwenden Sie rollenbasierte Zugriffskontrolle (RBAC), um granulare Berechtigungen auf Ressourcenebene zu gewähren.
3. Begrenzen Sie den Zugriff auf Storage-Konten und Ressourcen auf bestimmte Benutzer und Anwendungen.
4. Erstellen Sie getrennte Konten und Ressourcen für verschiedene Anwendungen oder Benutzergruppen.
5. W/R gemäss den Anforderungen erteilen. (Nur Upload = nur Write)
```

## CREATE RESOURCES WITH A SPECIFIC TAG
```
az group update --name <RESOURCE_GROUP_NAME> --set tags.Environment=Production tags.Department=Finance

costcenter: Verwendet, um den Kostenbereich zu identifizieren, der für die Abrechnung von Azure-Ressourcen verwendet wird.
environment: Verwendet, um die Umgebung (z.B. Produktion, Test oder Entwicklung) zu identifizieren, in der die Azure-Ressourcen verwendet werden.
owner: Verwendet, um den Eigentümer der Azure-Ressourcen zu identifizieren.
project: Verwendet, um das Projekt zu identifizieren, für das die Azure-Ressourcen verwendet werden.
```